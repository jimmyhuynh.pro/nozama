<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Produit;
use Knp\Component\Pager\PaginatorInterface;


class ProduitController extends AbstractController
{
    #[Route('/produit', name: 'app_produit')]
    public function produit( 
        PaginatorInterface $paginator,
        ManagerRegistry $doctrine,
        Request $request
    ): Response {

        $data = $doctrine->getRepository(Produit::class)->findAll();
        
        $produits = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('produit/index.html.twig', [
            'produits' => $produits,
        ]);
    
}

}