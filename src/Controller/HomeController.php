<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; 
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Produit;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ManagerRegistry $doctrine, Request $request, PaginatorInterface $paginator,): Response
    {   
        $results = [];
                
        if ($request->isMethod('POST')) { 
            $results = $doctrine->getRepository(Produit::class)->findBy(['title' => $request->get('search-bar-by-title')]);
            $results = $paginator->paginate(
                $results,
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );
        }
        else{
            $results = $doctrine->getRepository(Produit::class)->findAll();
            $results = $paginator->paginate(
                $results,
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );
        }
        
        return $this->render('home/index.html.twig', [
            'produits' => $results,
            'controller_name' => 'HomeController',
        ]);
    }

    #[Route('/about', name: 'app_about')]
    public function about(): Response
    {
        return $this->render('propos/about.html.twig', [
            'controller_name' => 'AboutController',
        ]);
    }
}

